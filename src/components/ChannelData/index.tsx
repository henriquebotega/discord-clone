import React, { useRef, useEffect } from "react";
import { Container, Messages, InputWrapper, InputIcon, Input } from "./styles";
import ChannelMessage, { Mention } from "../ChannelMessage";

const ChannelData: React.FC = () => {
	const messagesRef = useRef() as React.MutableRefObject<HTMLDivElement>;

	useEffect(() => {
		const div = messagesRef.current;

		if (div) {
			div.scrollTop = div.scrollHeight;
		}
	}, []);

	return (
		<Container>
			<Messages ref={messagesRef}>
				{Array.from(Array(15).keys()).map((n) => (
					<ChannelMessage
						author="Guilherme"
						date="21/06/2020"
						content="Hoje eh meu aniversario"
					/>
				))}

				<ChannelMessage
					author="Anderson"
					date="11/06/2020"
					content={
						<>
							<Mention>@Henrique</Mention>, Hoje voce nao vai trabalhar ?
						</>
					}
					hasMention
					isBot
				/>
			</Messages>

			<InputWrapper>
				<Input type="text" placeholder="Digite aqui sua frase" />
				<InputIcon />
			</InputWrapper>
		</Container>
	);
};

export default ChannelData;
