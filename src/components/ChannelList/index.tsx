import React from "react";
import { Container, Category, AddCategoryIcon } from "./styles";
import ChannelButton from "../ChannelButton";

const ChannelList: React.FC = () => {
	return (
		<Container>
			<Category>
				<span>Categoria</span>
				<AddCategoryIcon />
			</Category>

			<ChannelButton channelName="chat1" />
			<ChannelButton channelName="chat2" />
			<ChannelButton channelName="chat3" />
		</Container>
	);
};

export default ChannelList;
