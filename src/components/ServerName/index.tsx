import React from "react";
import { Container, Title, ExpandItem } from "./styles";

const ServerName: React.FC = () => {
	return (
		<Container>
			<Title>Servidor</Title>
			<ExpandItem />
		</Container>
	);
};

export default ServerName;
