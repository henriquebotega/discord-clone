import React from "react";
import {
	Container,
	HashtagIcon,
	Title,
	Description,
	Separator,
} from "./styles";

const ChannelInfo: React.FC = () => {
	return (
		<Container>
			<HashtagIcon />
			<Title>Chat livre</Title>
			<Separator />
			<Description>Aberto para conversas</Description>
		</Container>
	);
};

export default ChannelInfo;
